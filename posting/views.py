from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm

from . import forms, models
from .forms import PostForm
from .models import Post


def index(request):
    data = {
        'form':PostForm(),
        'posts' : models.Post.objects.order_by('date')
    }
    form = PostForm(request.POST)
    if form.is_valid():
        form.save()
        return redirect('index')

        
    return render(request, 'index.html', data)

# Create your views here.
def register(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')
    else :
        form = UserCreationForm()

    return render(request, 'register.html', {'form':form})

def login(request):

    return render(request, 'registration/login.html')