from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .models import Post
from .views import index
from .forms import PostForm
from time import sleep

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class Posting_unit_test(TestCase):
    def test_posting_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_posting_using_to_do_list_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_posting_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_posting_can_create_models(self):
        new_post = Post.objects.create(content="im fine")
        count = Post.objects.all().count()
        self.assertEqual(count, 1)
    
    def test_form(self):
        form_data = {
            'content':'hai ini content',
        }
        form = PostForm(data = form_data)
        self.assertTrue(form.is_valid())
        request = self.client.post('/', data = form_data)
        # self.assertEqual(request.status_code, 302)   

        response = self.client.get('/')
        self.assertEqual(request.status_code, 302)

class Functional_test(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        #HEROKU
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        #LOCAL
        # self.browser = webdriver.Chrome('posting/chromedriver.exe', chrome_options=chrome_options)
        super(Functional_test, self).setUp()
    
    def tearDown(self):
        self.browser.quit()
        super(Functional_test, self).tearDown()

    def test_input(self):
        self.browser.get(self.live_server_url)
        posts = self.browser.find_element_by_id('id_content')
        submit = self.browser.find_element_by_id('submit')
        posts.send_keys("Functional Testing")
        sleep(1)
        posts.send_keys(Keys.RETURN)
        sleep(2)
        # posts.submit()
        assert "Functional Testing" in self.browser.page_source


# Create your tests here.
