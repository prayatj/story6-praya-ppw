from django import forms
from .models import Post

class PostForm(forms.ModelForm):
    content = forms.CharField(max_length=50, required=True)
    class Meta:
        model = Post
        fields = '__all__'
